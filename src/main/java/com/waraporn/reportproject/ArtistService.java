/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.reportproject;

import java.util.List;

/**
 *
 * @author DELL
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(10);
    }
    
    public List<ArtistReport> getTopTenArtistByTotalPrice(String being, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(being, end, 10);
    }
}
